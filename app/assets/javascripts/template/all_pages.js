$(function() {

  // Mudar o tamanho do menu gerado pelo twitter-bootstrap-rails
  fixMenuContainer();

  // all .toggle links must have a rel attribute to identify what element will toggle
  $(document).on("click",'a.toggle', function() {
    $($(this).attr('rel')).toggle();
  });

  $('.datetimepicker').datetimepicker({
    lang:'pt-BR',
    format:'d/m/Y H:i',
    mask: true,
    step: 60
  });

  $('.datetimepicker_0').datetimepicker({
    lang:'pt-BR',
    format:'d/m/Y H:i',
    mask: true,
    step: 60
  });

  $('.datetimepicker_1').datetimepicker({
    lang:'pt-BR',
    format:'d/m/Y H:i',
    mask: true,
    step: 60
  });

  $('.datepicker').datetimepicker({
    lang:'pt-BR',
    timepicker:false,
    format:'d/m/Y',
    mask: true,
    closeOnDateSelect: true,
  });

  $('.datepicker_0').datetimepicker({
    lang:'pt-BR',
    timepicker:false,
    format:'d/m/Y',
    mask: true,
    closeOnDateSelect: true,
  });

  $('.datepicker_1').datetimepicker({
    lang:'pt-BR',
    timepicker:false,
    format:'d/m/Y',
    mask: true,
    closeOnDateSelect: true,
  });


  // Remove mensagem alert depois de 3 segundos
   setTimeout(function(){deleteSystemNotice()}, 3000);

  // Adicionar modalbox aos elementos
  $('#modal-element').on('hidden.bs.modal', function (e) {

     $(this).removeData('bs.modal');

     $('#som_alarme').remove();

     if($('.modal-dialog').hasClass('modal-lg')) {
       $('.modal-dialog').removeClass('modal-lg');
     }
   });

  $('#modal-element').on('show.bs.modal', function (e) {
     if($(e.relatedTarget).data('large')) {
       $('.modal-dialog').addClass('modal-lg');
     }
   });

  // Iniciar a função popover do Bootstrap.
  $('.com_popover').popover({
    html: true,
    trigger: 'hover',
    placement: 'left',
  });

  // open modalbox
  $('.lightbox, .fancybox').fancybox({
    helpers : {
      overlay : {
        autoSize: true
      }
    },
    afterShow: function() {
      $('.fancybox-inner').contents().find('input[type=text]:first').focus();
      $('.fancybox-wrap').swiperight(function() {
        $.fancybox.prev();
      });
      $('.fancybox-wrap').swipeleft(function() {
        $.fancybox.next();
      });
    },
  });

  $('.load-ajax').hide();
  $(document).ajaxStart(function() {
    $('.load-ajax').show();
  });
  $(document).ajaxStop(function() {
    $('.load-ajax').hide();
  });

});

function checkOrUncheckAll( to_be_checked, element_selector ){
  element_selector = element_selector || '.cid';
  var to_be_checked = to_be_checked || false;
  $( element_selector ).prop( "checked", to_be_checked ).trigger('change');
}

function defineAction( form, action, confirmation ) {
  var response = true;
  if( confirmation ) {
    response = confirm("Você tem certeza?");
  }
  if( response ) {
    $( "#" + form ).prop( "action", action);
    $( "#" + form ).submit();
  }
}

function defineOrder( form, action, id ) {
  $( "#" + form ).prop( "action", action);
  $( "#cb" + id ).prop( "checked", true);
  $( "#" + form ).submit();
}

function deleteSystemNotice() {
  $('.alert').slideUp('slow', function() {
    $('.alert').remove();
  });
}

function fixMenuContainer() {
  $('#menu div.container').removeClass('container').addClass('container-fluid');
}
