# -*- encoding : utf-8 -*-
class A::ConfiguracoesController < A::AppController

  skip_authorization_check
  skip_load_and_authorize_resource

  before_filter :garantir_autorizacao



  def gerais_salvar

    # CONFIGURACOES
    Settings['CONFIGURACOES.title']                                 = params[:title]
    Settings['CONFIGURACOES.logo']                                  = params[:logo]
    Settings['CONFIGURACOES.logo2']                                 = params[:logo2]
    Settings['CONFIGURACOES.logo_after']                            = params[:logo_after]
    Settings['CONFIGURACOES.logo2_after']                           = params[:logo2_after]

    # CONTATO
    Settings['CONTATO.telefone']            = params[:telefone]
    Settings['CONTATO.email']               = params[:email]
    Settings['CONTATO.mensagem']            = params[:mensagem]
    Settings['CONTATO.endereco']            = params[:endereco]
    Settings['CONTATO.atendimento_online']  = params[:atendimento_online]
    Settings['CONTATO.mapa_iframe']         = params[:mapa_iframe]

    # REDE_SOCIAIS
    Settings['REDE_SOCIAIS.site']       = params[:site]
    Settings['REDE_SOCIAIS.facebook']   = params[:facebook]

    @system_notice = I18n.t('configuracoes.salvou')
    @type = 'success'

    redirect_to a_configuracoes_gerais_path, notice: @system_notice
  end


  def sobre_salvar
    Settings['SOBRE.titulo']         = params[:titulo]
    Settings['SOBRE.conteudo']       = params[:conteudo]

    redirect_to a_configuracoes_sobre_path, notice: 'Configurações salvas com sucesso.'
  end


  def quem_somos_salvar
    Settings['QUEM_SOMOS.titulo']                   = params[:titulo]
    Settings['QUEM_SOMOS.conteudo']                 = params[:conteudo]

    
    redirect_to a_configuracoes_quem_somos_path, notice: 'Configurações salvas com sucesso.'
  end


  def gerar_api_token
    ApiKey.first.destroy
    ApiKey.create!

    respond_to do |format|
      format.js
    end
  end

  private

    def garantir_autorizacao
      authorize! :configurar, :sistema
    end

end
