# -*- encoding : utf-8 -*-
class A::ContatosController < A::AppController

  before_action :set_contato, only: [:show, :edit, :update, :destroy]

  # GET /contatos
  def index
    conditions = []
    conditions << "contatos.nome LIKE '%#{params[:nome]}%'" unless params[:nome].blank?
    conditions << "contatos.email LIKE '%#{params[:email]}%'" unless params[:email].blank?

    @contatos = Contato.where(conditions.join(' AND ')).ordenados.paginate page: params[:page], per_page: params[:per_page]

    # Mensagem para busca vazia
    if params[:commit] == t('helpers.links.search')
      if @contatos.count.zero?
        @system_notice = I18n.t('action_controller.nao_localizados', model: I18n.t('contatos.contatos'))
        @type = 'warning'
      end
    end

    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /contatos/1
  def show
    @reduzir = false
    if @contato.lido_em.nil?
      @contato.lido_em = Time.zone.now
      @contato.save(:validate => false)
      @reduzir = true
    end
    render layout: false
  end

  # GET /contatos/new
  def new
    @contato = Contato.new
  end

  # GET /contatos/1/edit
  def edit
  end

  # contato /contatos
  def create
    @contato = Contato.new(contato_params)

    if @contato.save
      redirect_to a_contatos_path, notice: "Contato Criado!"
    else
      render :new
    end
  end

  # PATCH/PUT /contatos/1
  def update

    if @contato.update(contato_params)
      redirect_to a_contatos_path, notice: "Contato Atualizado!"
    else
      render :edit
    end
  end

  # DELETE /contatos/1
  def destroy
    @contato.destroy
    redirect_to a_contatos_url, notice: notice
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contato
      @contato = Contato.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def contato_params
      params.require(:contato).permit(:nome, :telefone, :email, :mensagem)
    end

end