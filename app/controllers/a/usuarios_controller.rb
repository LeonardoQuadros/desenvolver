# -*- encoding : utf-8 -*-
class A::UsuariosController < A::AppController

  before_action :set_usuario, only: [:show, :edit, :update, :destroy, :alterar_senha]

  # GET /usuarios
  def index
    conditions = []
    conditions << "usuarios.nome LIKE '%#{params[:nome]}%'" unless params[:nome].blank?
    conditions << "usuarios.email LIKE '%#{params[:email]}%'" unless params[:email].blank?

    params[:ativos] = true if params[:ativos].blank?
    conditions << "usuarios.ativo = #{params[:ativos]}"

    @usuarios = Usuario.where(conditions.join(' AND ')).order("nome ASC").paginate page: params[:page], per_page: params[:per_page]

    # Mensagem para busca vazia
    if params[:commit] == t('helpers.links.search')
      if @usuarios.count.zero?
        @system_notice = I18n.t('action_controller.nao_localizados', model: I18n.t('usuarios.usuarios'))
        @type = 'warning'
      end
    end

    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /usuarios/1
  def show
    render layout: false
  end

  # GET /usuarios/new
  def new
    @usuario = Usuario.new
  end

  # GET /usuarios/1/edit
  def edit
  end

  # usuario /usuarios
  def create
    @usuario = Usuario.new(usuario_params)

    if @usuario.save
      redirect_to a_usuarios_path, notice: I18n.t('action_controller.adicionado', model: I18n.t('usuarios.usuario'))
    else
      render :new
    end
  end

  # PATCH/PUT /usuarios/1
  def update

    # Se o password não foi preenchido, não atualiza
    if params[:usuario][:password].blank?
      params[:usuario].delete :password
      params[:usuario].delete :password_confirmation
    end

    if @usuario.update(usuario_params)
      redirect_to a_usuarios_path, notice: I18n.t('action_controller.atualizado', model: I18n.t('usuarios.usuario'))
    else
      render :edit
    end
  end

  # DELETE /usuarios/1
  def destroy
    @usuario.alterar!
    @usuario.ativo? ? (notice = I18n.t('action_controller.ativado', model: I18n.t('usuarios.usuario'))) : (notice = I18n.t('action_controller.desativado', model: I18n.t('usuarios.usuario')))
    redirect_to a_usuarios_url, notice: notice
  end

  def alterar_senha
    render layout: false
  end

  def salvar_senha
    @usuario = current_a_usuario

    senha_valida = @usuario.valid_password? params[:usuario][:password_atual]
    @usuario.errors.add(:password_atual, I18n.t('usuarios.password_invalida')) unless senha_valida

    respond_to do |format|
      if senha_valida and @usuario.update(usuario_params)
        sign_in(current_a_usuario, bypass: true)
        @system_notice = I18n.t('action_controller.atualizada', model: I18n.t('usuarios.password'))
        @type = 'info'
        format.js
      else
        format.js
      end
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_usuario
      @usuario = Usuario.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def usuario_params
      params.require(:usuario).permit(:nome, :telefone, :celular, :email, :password, :password_confirmation, :ativo, :super_admin)
    end

end