# -*- encoding : utf-8 -*-
class ContatosController < PublicController

  def new
    @contato = Contato.new
  end

  def create
    @contato = Contato.new(contato_params)

    respond_to do |format|
      if @contato.save
        @system_notice = Settings['CONTATO.mensagem'].to_s
        @type = 'success'
        Mailer.delay.contato @contato
      else
        @system_notice = "Alguma coisa deu errada.".to_s
      end
        format.js
    end
  end

  private
    # Only allow a trusted parameter "white list" through.
    def contato_params
      params.require(:contato).permit(:mensagem, :remote_ip, :user_agent, :nome, :telefone, :email)
    end

end