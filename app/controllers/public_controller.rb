# -*- encoding : utf-8 -*-
class PublicController < ApplicationController

  layout 'site'

  skip_authorization_check
  skip_load_and_authorize_resource


end
