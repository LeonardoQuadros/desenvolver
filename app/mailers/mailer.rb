# -*- encoding : utf-8 -*-
class Mailer < ActionMailer::Base
  default from: "no-reply@cinco_capital.com.br"

  # Enviar link para reset de senha, informações de record e token são geradas pelo device
  def reset_password_instructions(record, token, opts={})
    @token = token
    @resource = record

    mail( to: @resource.email, subject: "Instruções para recuperação de senha") do |format|
      format.html
    end
  end

  # Enviar e-mail de contato
  def contato (contato, options = {})
    @options = options
    @contato = contato
    @contato.enviado!
     mail( to: Settings['CONTATO.email'], subject: "#{Settings['CONFIGURACOES.title']} - Solicitação de Contato - #{@contato.nome}" ) do |format|
      format.html
    end
  end

  # Enviar e-mail de contato
  def contato_corretor (contato, options = {})
    @options = options
    @contato = contato
    cliente_id = @contato.cliente_id
    @corretor = Cliente.where(id: cliente_id).first
    unless @corretor.blank? or @corretor.email.blank?
      @contato.enviado!
       mail( to: @corretor.email, subject: "#{Settings['CONFIGURACOES.title']} - Solicitação de Serviço - #{@contato.nome}" ) do |format|
        format.html
      end
    end
  end

  # Enviar e-mail de contato
  def cliente (cliente, options = {})
    @options = options
    @cliente = cliente
       mail( to: @cliente.email, subject: "#{Settings['CONFIGURACOES.title']} - Confirmação de Cadastro - #{@cliente.nome}" ) do |format|
        format.html
      end
  end

  # Enviar e-mail de contato
  def lead (cliente, options = {})
    @options = options
    @cliente = cliente
       mail( to: @cliente.email, subject: "#{Settings['CONFIGURACOES.title']} - Confirmação de Cadastro - #{@cliente.nome}" ) do |format|
        format.html
      end
  end


  # Enviar e-mail de contato
  def corretor (corretor, options = {})
    @options = options
    @corretor = corretor
       mail( to: @corretor.email, subject: "#{Settings['CONFIGURACOES.title']} - Confirmação de Cadastro - #{@corretor.nome}" ) do |format|
        format.html
      end
  end


  # Enviar e-mail de contato
  def confirmacao_corretor (corretor, options = {})
    @options = options
    @corretor = corretor
       mail( to: @corretor.email, subject: "#{Settings['CONFIGURACOES.title']} - Confirmação de Corretor - #{@corretor.nome}" ) do |format|
        format.html
      end
  end


  # Enviar e-mail de contato
  def imovel (cliente, produto, options = {})
    @options = options
    @cliente = cliente
    @produto = produto
       mail( to: @cliente.email, subject: "#{Settings['CONFIGURACOES.title']} - Confirmação de Imóvel - #{@produto.titulo}" ) do |format|
        format.html
      end
  end


  # Enviar e-mail  para construtora
  def construtora (proposta, options = {})
    @options = options
    @proposta = proposta
    @contrato = Contrato.where(produto_id: proposta.produto_id).first
    unless @contrato.blank?
        mail( to: @contrato.email, subject: "#{Settings['CONFIGURACOES.title']} - Nova Proposta - #{proposta.produto.titulo}" ) do |format|
          format.html
        end
    end
  end


  # Enviar e-mail  para construtora
  def imobiliaria_proposta (proposta, options = {})
    @options = options
    @proposta = proposta
    @contrato = Contrato.where(produto_id: proposta.produto_id).first
    unless @contrato.blank?
        mail( to: Settings['CONTATO.email'], subject: "#{Settings['CONFIGURACOES.title']} - Proposta - #{proposta.produto.titulo}" ) do |format|
          format.html
        end
    end
  end


  # Enviar e-mail  para construtora
  def pesquisa_satisfacao (contato, avaliacao, options = {})
    @options = options
    @contato = contato
    @avaliacao = avaliacao
    unless @avaliacao.blank?
      mail( to: @contato.email, subject: "#{Settings['CONFIGURACOES.title']} - #{contato.nome_completo}" ) do |format|
        format.html
      end
    end
  end

end
