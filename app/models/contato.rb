# -*- encoding : utf-8 -*-
class Contato < ActiveRecord::Base

  validates :nome, :email, :telefone, presence: true


  scope :ordenados, -> {order('contatos.created_at ASC')}
  scope :nao_lidos, -> {where('contatos.lido_em IS NULL')}


  def self.to_select
    Contato.ordenados.collect { |u| [u.nome, u.id] }
  end


  def self.media_dia data
      inicio = data.beginning_of_day
      fim = data.beginning_of_day + 1.day
      contatos = Contato.where('created_at BETWEEN ? AND ?', inicio.to_s(:db), fim.to_s(:db)).size
  end
  
end