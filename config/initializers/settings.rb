# -*- encoding : utf-8 -*-

# Configuração de formatos padrão de Date e Time
Date::DATE_FORMATS[:default]="%d/%m/%Y"
Time::DATE_FORMATS[:default]="%d/%m/%Y %H:%M"

if Settings.table_exists?

  # Configurações Gerais
  Settings.defaults['CONFIGURACOES.title']                      = "CINCO CAPITAL"
  Settings.defaults['CONFIGURACOES.logo']                       = "/logo.png"
  Settings.defaults['CONFIGURACOES.logo_after']                 = "/logo2.png"


  # Configurações Sobre
  Settings.defaults['SOBRE.titulo']         = 'O Que Fazemos?'
  Settings.defaults['SOBRE.conteudo']       = 'Formada por uma equipe de especialistas em mercado financeiro, a Desenvolver tem como objetivo ajudar seus clientes, de forma simples e inteligente, a atingirem seus objetivos empreendedores.'

  # Configurações Quem Somos
  Settings.defaults['QUEM_SOMOS.titulo']         = 'O que é Desenvolver'
  Settings.defaults['QUEM_SOMOS.conteudo']       = 'Empreender é ter autonomia para usar as melhores competências para criar algo diferente e com valor, com comprometimento, pela dedicação de tempo e esforço necessários, assumindo os riscos financeiros, físicos e sociais.'


  # Configurações Contato
  Settings.defaults['CONTATO.telefone']           = ''
  Settings.defaults['CONTATO.email']              = 'contato@desenvolver.com.br'
  Settings.defaults['CONTATO.mensagem']           = 'Obrigado por entrar em contato. Retornaremos o mais breve possível.'
  Settings.defaults['CONTATO.endereco']           = ' R. Padre Teixeira, 2475 - Jardim Bethania, São Carlos - SP, 13561-050'
  Settings.defaults['CONTATO.atendimento_online'] = ''
  Settings.defaults['CONTATO.mapa_iframe']        = ''
  Settings.defaults['CONTATO.meta_descricao']     = ''
  Settings.defaults['CONTATO.meta_script']        = ''
  Settings.defaults['CONTATO.submit_meta_script'] = ''

  # Cadastro de Redes Sociais
  Settings.defaults['REDE_SOCIAIS.site']  = "http://www.desenvolver.com.br/"
  Settings.defaults['REDE_SOCIAIS.facebook']  = "https://www.facebook.comImoveisOficial"


end
