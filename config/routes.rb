# -*- encoding : utf-8 -*-

require 'api_constraints'

Rails.application.routes.draw do

  mount Ckeditor::Engine => '/ckeditor'

  #########################################
  ############## Aplicação ################
  #########################################
  namespace :a do
    devise_for :usuarios, class_name: "Usuario", controllers: { sessions: 'a/usuarios/sessions',
                                                                passwords: 'a/usuarios/passwords',
                                                                registrations: 'a/usuarios'  }
    controller :usuarios do
      get "usuarios/alterar_senha" => :alterar_senha
      post "usuarios/salvar_senha" => :salvar_senha
    end
    resources :usuarios


    controller :contatos do
    end
    resources :contatos

    controller :configuracoes do
      get '/configuracoes/gerais' => :gerais, as: :configuracoes_gerais
      post '/configuracoes/gerais_salvar' => :gerais_salvar, as: :configuracoes_gerais_salvar
      get '/configuracoes/sobre' => :sobre, as: :configuracoes_sobre
      post '/configuracoes/sobre_salvar' => :sobre_salvar, as: :configuracoes_sobre_salvar
      get '/configuracoes/quem_somos' => :quem_somos, as: :configuracoes_quem_somos
      post '/configuracoes/quem_somos_salvar' => :quem_somos_salvar, as: :configuracoes_quem_somos_salvar
      get 'configuracoes/gerar_api_token' => :gerar_api_token
    end

    root "contatos#index"
  end

  #########################################
  ################ Public #################
  #########################################

  controller :contatos do
  end
  resources :contatos
  
  controller :pages do
    get '/home' => :home
  end

  root "pages#home"

  #########################################
  ################# API ###################
  #########################################
  namespace :api, defaults: {format: 'json'} do
    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: true) do

      controller :sync do
        get 'sync' => :sync
      end

    end
  end

end