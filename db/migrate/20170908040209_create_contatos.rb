class CreateContatos < ActiveRecord::Migration
  def change
    create_table :contatos do |t|
    	t.string :nome
    	t.string :email
    	t.string :telefone
    	t.string :celular
    	t.text :mensagem
    	t.string :remote_ip
    	t.string :user_agent
    	t.datetime :lido_em

    	t.timestamps
    end
  end
end
